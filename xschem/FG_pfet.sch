v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 130 -40 150 -40 { lab=vd}
N 150 -90 150 -40 { lab=vd}
N 130 -90 150 -90 { lab=vd}
N 130 -90 130 -70 { lab=vd}
N 130 -10 130 20 { lab=vsource}
N -20 -40 90 -40 { lab=#net1}
N 30 -100 30 -40 { lab=#net1}
N 0 -170 -0 -140 { lab=vtun}
N -0 -170 30 -170 { lab=vtun}
N 30 -170 30 -140 { lab=vtun}
N 60 -170 60 -140 { lab=vtun}
N 30 -170 60 -170 { lab=vtun}
N 30 -200 30 -170 { lab=vtun}
N -80 -70 -60 -70 { lab=vin}
N -80 -70 -80 -40 { lab=vin}
N -80 -40 -60 -40 { lab=vin}
N -80 -10 -60 -10 { lab=vin}
N -80 -40 -80 -10 { lab=vin}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 110 -40 0 0 {name=M1
L=0.5
W=2
nf=1 mult=1
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} 30 -120 3 0 {name=M2
L=0.5
W=1
nf=1 mult=1
model=pfet_g5v0d10v5
spiceprefix=X
}
C {sky130_fd_pr/pfet_g5v0d10v5.sym} -40 -40 2 0 {name=M3
L=3
W=2
nf=1 mult=1
model=pfet_g5v0d10v5
spiceprefix=X
}
C {devices/lab_pin.sym} 130 -90 0 0 {name=l1 sig_type=std_logic lab=vd}
C {devices/lab_pin.sym} 30 -200 0 0 {name=l2 sig_type=std_logic lab=vtun}
C {devices/lab_pin.sym} -80 -50 0 0 {name=l3 sig_type=std_logic lab=vin}
C {devices/lab_pin.sym} 130 20 0 0 {name=l4 sig_type=std_logic lab=vsource}
C {devices/iopin.sym} -240 -140 0 0 {name=p1 lab=vtun
}
C {devices/iopin.sym} -240 -110 0 0 {name=p2 lab=vin
}
C {devices/iopin.sym} -240 -80 0 0 {name=p3 lab=vsource
}
C {devices/iopin.sym} -240 -60 0 0 {name=p4 lab=vd}
