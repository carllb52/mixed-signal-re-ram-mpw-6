v {xschem version=2.9.9 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 0 170 0 230 { lab=#net1}
N -110 170 110 170 { lab=#net1}
N 110 30 110 110 { lab=#net2}
N -110 30 -110 110 { lab=#net3}
N -420 -230 420 -230 { lab=VPWR}
N -110 -170 -110 -30 { lab=#net4}
N 110 -170 110 -30 { lab=#net5}
N -420 -170 -420 80 { lab=#net3}
N -420 80 -110 80 { lab=#net3}
N 420 -170 420 80 { lab=#net2}
N 110 80 420 80 { lab=#net2}
N -290 -170 -290 -140 { lab=#net4}
N -290 -140 -110 -140 { lab=#net4}
N 290 -170 290 -140 { lab=#net5}
N 110 -140 290 -140 { lab=#net5}
N -330 -200 -330 -140 { lab=CLK}
N -460 -140 -330 -140 { lab=CLK}
N -460 -200 -460 -140 { lab=CLK}
N 330 -200 330 -140 { lab=CLK}
N 330 -140 460 -140 { lab=CLK}
N 460 -200 460 -140 { lab=CLK}
N -70 -200 70 -140 { lab=#net5}
N 70 -140 110 -140 { lab=#net5}
N -70 0 70 -60 { lab=#net5}
N 70 -60 110 -60 { lab=#net5}
N -70 -60 70 0 { lab=#net4}
N -110 -60 -70 -60 { lab=#net4}
N -70 -140 70 -200 { lab=#net4}
N -110 -140 -70 -140 { lab=#net4}
N 620 130 620 170 { lab=#net6}
N 620 -170 620 -130 { lab=#net7}
N 580 -200 580 -100 { lab=#net5}
N 580 100 580 200 { lab=#net4}
N 110 -120 580 -120 { lab=#net5}
N -110 -80 500 -80 { lab=#net4}
N 500 -80 500 140 { lab=#net4}
N 500 140 580 140 { lab=#net4}
N 620 -150 760 -150 { lab=#net7}
N 760 -150 760 -100 { lab=#net7}
N 760 -100 800 -100 { lab=#net7}
N 620 150 1120 150 { lab=#net6}
N 1120 -100 1120 150 { lab=#net6}
N 1080 -100 1120 -100 { lab=#net6}
N 840 -200 840 -130 { lab=X}
N 1040 -200 1040 -130 { lab=#net8}
N 880 -230 1000 -180 { lab=#net8}
N 1000 -180 1040 -180 { lab=#net8}
N 880 -180 1000 -230 { lab=X}
N 840 -180 880 -180 { lab=X}
N 840 -260 1040 -260 { lab=VPWR}
N -420 120 -420 200 { lab=VGND}
N -420 200 -360 200 { lab=VGND}
N -360 120 -360 200 { lab=VGND}
N 360 120 360 200 { lab=VGND}
N 360 200 420 200 { lab=VGND}
N 420 120 420 200 { lab=VGND}
N -320 -100 -320 -20 { lab=VGND}
N -320 -20 -260 -20 { lab=VGND}
N -260 -100 -260 -20 { lab=VGND}
N 260 -100 260 -20 { lab=VGND}
N 260 -20 320 -20 { lab=VGND}
N 320 -100 320 -20 { lab=VGND}
C {devices/ipin.sym} 0 -450 0 0 {name=p4 lab=CLK}
C {devices/ipin.sym} -150 140 0 0 {name=p1 lab=VIN1}
C {devices/ipin.sym} 150 140 0 1 {name=p3 lab=VIN2}
C {devices/ipin.sym} 0 -420 0 0 {name=p5 lab=VGND}
C {devices/ipin.sym} 0 -390 0 0 {name=p6 lab=VNB}
C {devices/ipin.sym} 0 -360 0 0 {name=p7 lab=VPB}
C {devices/ipin.sym} 0 -330 0 0 {name=p8 lab=VPWR}
C {devices/opin.sym} 840 -150 0 1 {name=p2 lab=X}
C {sky130_fd_pr/nfet_01v8.sym} -20 260 0 0 {name=M1
L=0.15
W=1.49
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} -440 -200 0 0 {name=M2
L=0.15
W=0.45
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 0 -450 0 1 {name=l1 sig_type=std_logic lab=CLK}
C {devices/lab_pin.sym} 0 -420 0 1 {name=l2 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} 0 -390 0 1 {name=l3 sig_type=std_logic lab=VNB}
C {devices/lab_pin.sym} 0 -360 0 1 {name=l4 sig_type=std_logic lab=VPB}
C {devices/lab_pin.sym} 0 -330 0 1 {name=l5 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} -40 260 0 0 {name=l6 sig_type=std_logic lab=CLK}
C {devices/lab_pin.sym} 0 290 1 1 {name=l7 sig_type=std_logic lab=VGND}
C {sky130_fd_pr/nfet_01v8.sym} -130 140 0 0 {name=M3
L=0.15
W=1
nf=1 
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 130 140 0 1 {name=M4
L=0.15
W=1
nf=1 
mult=4
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 90 0 0 0 {name=M5
L=0.15
W=0.7
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} -90 0 0 1 {name=M6
L=0.15
W=0.7
nf=1 
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} -310 -200 0 0 {name=M7
L=0.15
W=0.45
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} -90 -200 0 1 {name=M8
L=0.15
W=0.63
nf=1
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 90 -200 0 0 {name=M9
L=0.15
W=0.63
nf=1
mult=2
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 310 -200 0 1 {name=M10
L=0.15
W=0.45
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 440 -200 0 1 {name=M11
L=0.15
W=0.45
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 0 -230 3 1 {name=l8 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} 460 -200 0 1 {name=l9 sig_type=std_logic lab=CLK}
C {devices/lab_pin.sym} -460 -200 0 0 {name=l10 sig_type=std_logic lab=CLK}
C {devices/lab_pin.sym} -420 -200 0 1 {name=l11 sig_type=std_logic lab=VPB}
C {devices/lab_pin.sym} -290 -200 0 1 {name=l12 sig_type=std_logic lab=VPB}
C {devices/lab_pin.sym} 110 -200 0 1 {name=l13 sig_type=std_logic lab=VPB}
C {devices/lab_pin.sym} -110 -200 0 0 {name=l14 sig_type=std_logic lab=VPB}
C {devices/lab_pin.sym} 290 -200 0 0 {name=l15 sig_type=std_logic lab=VPB}
C {devices/lab_pin.sym} 420 -200 0 0 {name=l16 sig_type=std_logic lab=VPB}
C {devices/lab_pin.sym} 110 0 0 1 {name=l17 sig_type=std_logic lab=VNB}
C {devices/lab_pin.sym} -110 140 0 1 {name=l18 sig_type=std_logic lab=VNB}
C {devices/lab_pin.sym} 0 260 0 1 {name=l19 sig_type=std_logic lab=VNB}
C {devices/lab_pin.sym} -110 0 0 0 {name=l20 sig_type=std_logic lab=VNB}
C {devices/lab_pin.sym} 110 140 0 0 {name=l21 sig_type=std_logic lab=VNB}
C {sky130_fd_pr/nfet_01v8.sym} 600 -100 0 0 {name=M12
L=0.15
W=0.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 600 -200 0 0 {name=M13
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 600 200 0 0 {name=M14
L=0.15
W=0.65
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 600 100 0 0 {name=M15
L=0.15
W=1
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 820 -100 0 0 {name=M16
L=0.15
W=0.645
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/nfet_01v8.sym} 1060 -100 0 1 {name=M17
L=0.15
W=0.645
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 620 230 1 1 {name=l22 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} 620 -70 1 1 {name=l23 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} 840 -70 1 1 {name=l24 sig_type=std_logic lab=VGND}
C {devices/lab_pin.sym} 1040 -70 1 1 {name=l25 sig_type=std_logic lab=VGND}
C {sky130_fd_pr/pfet_01v8.sym} 860 -230 0 1 {name=M18
L=0.15
W=0.7
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {sky130_fd_pr/pfet_01v8.sym} 1020 -230 0 0 {name=M19
L=0.15
W=0.7
nf=1
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=pfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 940 -260 3 1 {name=l26 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} 620 -230 3 1 {name=l27 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} 620 70 3 1 {name=l28 sig_type=std_logic lab=VPWR}
C {devices/lab_pin.sym} 840 -230 0 0 {name=l29 sig_type=std_logic lab=VPB}
C {devices/lab_pin.sym} 620 -200 0 1 {name=l30 sig_type=std_logic lab=VPB}
C {devices/lab_pin.sym} 620 100 0 1 {name=l31 sig_type=std_logic lab=VPB}
C {devices/lab_pin.sym} 1040 -230 0 1 {name=l32 sig_type=std_logic lab=VPB}
C {devices/lab_pin.sym} 1040 -100 0 0 {name=l33 sig_type=std_logic lab=VNB}
C {devices/lab_pin.sym} 840 -100 0 1 {name=l34 sig_type=std_logic lab=VNB}
C {devices/lab_pin.sym} 620 -100 0 1 {name=l35 sig_type=std_logic lab=VNB}
C {devices/lab_pin.sym} 620 200 0 1 {name=l36 sig_type=std_logic lab=VNB}
C {sky130_fd_pr/nfet_01v8.sym} -390 100 1 0 {name=M20
L=1
W=1.49
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} -390 120 1 1 {name=l37 sig_type=std_logic lab=VNB}
C {devices/lab_pin.sym} -390 200 1 1 {name=l38 sig_type=std_logic lab=VGND}
C {sky130_fd_pr/nfet_01v8.sym} 390 100 1 0 {name=M21
L=1
W=1.49
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 390 120 1 1 {name=l39 sig_type=std_logic lab=VNB}
C {devices/lab_pin.sym} 390 200 1 1 {name=l40 sig_type=std_logic lab=VGND}
C {sky130_fd_pr/nfet_01v8.sym} -290 -120 1 0 {name=M22
L=1
W=1.49
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} -290 -100 1 1 {name=l41 sig_type=std_logic lab=VNB}
C {devices/lab_pin.sym} -290 -20 1 1 {name=l42 sig_type=std_logic lab=VGND}
C {sky130_fd_pr/nfet_01v8.sym} 290 -120 1 0 {name=M23
L=1
W=1.49
nf=1 
mult=1
ad="'int((nf+1)/2) * W/nf * 0.29'" 
pd="'2*int((nf+1)/2) * (W/nf + 0.29)'"
as="'int((nf+2)/2) * W/nf * 0.29'" 
ps="'2*int((nf+2)/2) * (W/nf + 0.29)'"
nrd="'0.29 / W'" nrs="'0.29 / W'"
sa=0 sb=0 sd=0
model=nfet_01v8
spiceprefix=X
}
C {devices/lab_pin.sym} 290 -100 1 1 {name=l43 sig_type=std_logic lab=VNB}
C {devices/lab_pin.sym} 290 -20 1 1 {name=l44 sig_type=std_logic lab=VGND}
